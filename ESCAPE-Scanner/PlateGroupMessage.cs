﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESCAPE_Scanner
{
    public class PlateGroupMessage
    {
        public string CameraId { get; set; }
        public string Plate { get; set; }
        public double Confidence { get; set; }
        public string JpgImageBase64 { get; set; }
    }
}
