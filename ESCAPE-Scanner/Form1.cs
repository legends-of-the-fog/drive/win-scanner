﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ESCAPE_Scanner
{
    public partial class Form1 : Form
    {
        SoundPlayer _player = new SoundPlayer();
        HubConnection connection;
        string currentZone = null;
        public Form1()
        {
            InitializeComponent();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                var hostname = "https://fog.enderlab.com";
                if (Debugger.IsAttached)
                    hostname = "https://laptop.enderlab.com";

                //simply prime the cookie container
                var cc = new CookieContainer();
                HttpClient hc = new HttpClient(new HttpClientHandler() { CookieContainer = cc, UseCookies = true });
                await hc.GetAsync(hostname);

                //connection use the primed cookie container
                connection = new HubConnectionBuilder()
                    .WithUrl(hostname + "/admin/adminClient", opt =>
                    {
                        opt.Cookies = cc;
                        opt.Headers.Add("X-FOG-API", "iknowu");
                    })
                    .WithAutomaticReconnect(Enumerable.Repeat(TimeSpan.FromSeconds(3), 1000).ToArray())
                    .Build();

                _player.SoundLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/bell.wav";
                _player.Load();

                connection.Reconnected += async (a) =>
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        toolStripStatusLabel1.Text = "Connected";
                        Ready();
                    }));
                    await SetZone();
                };

                connection.Reconnecting += (a) =>
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        groupBox1.Enabled = false;
                        toolStripStatusLabel1.Text = "Reconnecting";
                        label3.Text = "CONNECTING";
                    }));
                    return Task.CompletedTask;
                };

                connection.Closed += (error) =>
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        toolStripStatusLabel1.Text = "Disconnected";
                    }));
                    return Task.CompletedTask;
                };

                connection.On<PlateGroupMessage>("GroupPlateSeen", PlateSeen);

                await connection.StartAsync();
                await SetZone();
                timer1.Start();
                Ready();
                toolStripStatusLabel1.Text = "Connected";
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Error");
                toolStripStatusLabel1.Text = "Error";
                groupBox1.Enabled = false;
                No("Error");
            }
        }

        private void Ready()
        {
            panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            label3.Text = "Ready".ToUpper();
            groupBox1.Enabled = true;
            textBox1.Focus();
        }

        private async Task SetZone()
        {
            if (currentZone != null)
                await connection.InvokeAsync("RemoveFromGroup", currentZone);

            if (comboBox1.SelectedItem == null)
                return;

            currentZone = comboBox1.SelectedItem.ToString();
            await connection.InvokeAsync("AddToGroup", currentZone);
        }

        Random _r = new Random();
        private void PlateSeen(PlateGroupMessage msg)
        {
            Invoke(new MethodInvoker(() =>
            {
                try
                {
                    pictureBox1.Image = Image.FromStream(new MemoryStream(Convert.FromBase64String(msg.JpgImageBase64)));
                }
                catch (Exception)
                {
                }
                Scan("SCAN");
                label4.Text = msg.Plate?.ToUpper();
                _player.Play();
            }));
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var code = textBox1.Text;
                textBox1.Clear();
                textBox1.Focus();
                var b = await connection.InvokeAsync<bool>("LinkPlateToDriveId", code, currentZone);
                if (b)
                {
                    Yes("Scare");
                }
                else
                    No("Invalid");
            }
            catch (Exception ex)
            {
                No("Err");
            }
        }

        private void Scan(string v)
        {
            panel1.BackColor = Color.Yellow;
            label3.Text = v.ToUpper();
            //ScheduleReady();
        }

        private void Yes(string v)
        {
            panel1.BackColor = Color.Green;
            label3.Text = v.ToUpper();
            ScheduleReady();
        }

        private void No(string v)
        {
            panel1.BackColor = Color.Red;
            label3.Text = v.ToUpper();
            ScheduleReady();
        }

        private void ScheduleReady()
        {
            timer1.Enabled = true;
            timer1.Start();
            groupBox1.Enabled = false;
        }

        private async void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            await SetZone();
            button1.Enabled = true;
            textBox1.Enabled = true;
            textBox1.Focus();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            timer1.Stop();
            Ready();
        }
    }
}
